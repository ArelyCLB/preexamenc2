const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser");


router.get("/", (req,res)=>{
    const params ={
        num : req.query.num,
        destino : req.query.destino,
        nom : req.query.nom,
        año : req.query.año,
        tipo : req.query.tipo,
        valor : req.query.valor,
    }
    res.render('pre-exa.html');
})


module.exports = router;